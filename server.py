import core, intervals
from flask import Flask, jsonify, request, redirect, abort
from intervals import min2time


app = Flask(__name__)
API_PREFIX = "/api/v1"


@app.route('/')
@app.route('/static')
def LTHome():
    return redirect("/static/index.html")


@app.route(API_PREFIX + '/temp')
@app.route(API_PREFIX + '/temperature')
def getCurrTemp():
    return str(core.read_temp())


@app.route(API_PREFIX + '/target')
def getCurrTarget():
    return str(intervals.get_curr_target())


@app.route(API_PREFIX + '/server_clock')
def getServerClock():
    return intervals.get_server_clock()


@app.route(API_PREFIX + '/intervals')
def getIntervals():
    i = intervals.intervals
    res = []

    for day in range(len(i))    :
        day_program = {
            'day': day,
            'intervals': []
        }
        for x in i[day]:
            day_program['intervals'].append({
                'start': "{0[0]}:{0[1]}".format(min2time(x[0])),
                'end': "{0[0]}:{0[1]}".format(min2time(x[1])),
                'temp': x[2]
            })
        res.append(day_program)

    return jsonify(res)


@app.route(API_PREFIX + '/intervals', methods=['POST'])
def setIntervals():
    print(request.form)
    r = request.form
    w, s, e, t = r.get('day'), r.get('start'), r.get('end'), r.get('temp')
    return "OK" if intervals.add_interval(w, (s, e, t)) else abort(406)


@app.route(API_PREFIX + '/intervals', methods=['DELETE'])
def delIntervals():
    day = int(request.form['day'])
    print(request.form, day)
    return "OK" if intervals.reset_day(day) else abort(406)


def init():
    app.run('0.0.0.0', 5000, debug=False)
