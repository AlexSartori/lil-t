from datetime import datetime

# Intervals[week_day] = (start, end, temp)


def min2time(minutes):
    return (int("{:02d}".format(int(minutes/60)%24)),
            int("{:02d}".format(minutes%60)))


def time2min(t):
    if type(t) is str: t = t.split(":")
    return int(t[0])*60 + int(t[1])


def add_interval(w, x):
    global intervals

    if w is None or x is None:
        print("None parameter:", w, x)
        return False
    if len(x) != 3:
        print("Wrong interval format:", x)
        return False

    try:
        x = (time2min(x[0]), time2min(x[1]), float(x[2]))
    except:
        print("Wrong interval format:", x)
        return False

    w = int(w)
    intervals[w].append(x)
    intervals[w].sort(key=lambda x:x[0])

    for i in range(len(intervals[w]) - 1):
        a, b = intervals[w][i], intervals[w][i+1]
        if a[1] != b[0]:
            old_end = a[1]
            intervals[w][i] = (a[0], b[0], a[2]) # Il primo finisce quando inizia l'interno
            intervals[w].append((b[1], old_end, a[2])) # Il nuovo inzia quando finisce l'interno e finisce con old_end (temp di a)
            break

    intervals[w].sort(key=lambda x:x[0])
    return True


def reset_day(day):
    global intervals

    if day < 0 or day > 6:
        print("Invalid day [0-6]:", day)
        return False

    intervals[day] = [(time2min((0,0)), time2min((23,59)), 0)]
    return True


def get_curr_target():
    date_now = datetime.now()
    w = date_now.weekday() # From 0 to 6 apparently
    now = date_now.hour * 60 + date_now.minute

    for x in intervals[w]:
        if x[0] <= now and x[1] >= now:
            return float(x[2])

    return 0

def get_server_clock():
    n = datetime.now()
    return "{:02d}:{:02d}".format(n.hour, n.minute)


'''
def get_contextual_sleep():
    date_now = datetime.now()
    w = date_now.weekday() # From 0 to 6 apparently
    now = date_now.hour * 60 + date_now.minute

    next_change = now
    target = 0.0
    for x in intervals[w]:
        if x[0] <= now and x[1] >= now:
            next_change = x[1]
            target = float(x[2])
            break

    if target == 0:
        # Sleep until necessary
        return (next_change - now)*60
    else:
        # TODO Dormi più o meno in base a media esponenziale temperatura e progressione
        return 7*60
'''


def print_intervals():
    for d in range(7):
        print("\n{:11}".format(['Monday', "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"][d] + ":"))
        for (s, e, t) in intervals[d]:
            print(
                "\t" + min2time(s)[0] + ":" + min2time(s)[1] +
                " - " + min2time(e)[0] + ":" + min2time(e)[1] +
                " -> " + str(t) + "°C"
            )


intervals = []
def init():
    global intervals

    print(" * [Ints] Initializing intervals...")
    for i in range(7):
        intervals.append([])
        reset_day(i)
