echo "#### PWD: "
pwd

echo "### WHOAMI: "
whoami

echo "[*] Cleaning old stuff..."
rm -rf lil-t

echo "[*] Pulling latest version..."
git clone http://bitbucket.org/AlexSartori/lil-t

echo "[*] Updating bootstrap script..."
mv lil-t/boot_lil-t.sh .

echo "[*] Booting the server..."
cd lil-t && sudo python3.6 main_loop.py
