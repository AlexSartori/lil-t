import core, server, intervals


if __name__ == '__main__':
    intervals.init()
    core.init()
    server.init()
