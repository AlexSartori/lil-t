import _thread, time, os, glob
import intervals
import RPi.GPIO as GPIO


ADAFRUIT_IO_KEY = "5fa2f40b6e694ec5b02c89e3f080e12d"
GPIO_LED = 23
GPIO_RELAY = 24

# W1 temp sensor data
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'


def init():
    print(" * [Core] Initializing...")
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(GPIO_LED, GPIO.OUT)
    GPIO.setup(GPIO_RELAY, GPIO.OUT)
    _thread.start_new_thread(thread_run, ())


def thread_run():
    while True:
        target = intervals.get_curr_target()
        if target != 0:
            t = read_temp()
            if t == "FAIL":
                print("O BOI DUNNO THE TEMP HALP")
                sleep(10)
                break

            if float(t) <= target - 0.5:
                switch_on()
            elif float(t) >= target + 1:
                switch_off()

            send_temp(t)

        time.sleep(10*60)


def switch_on():
    GPIO.output(GPIO_LED, GPIO.HIGH)
    GPIO.output(GPIO_RELAY, GPIO.HIGH)


def switch_off():
    GPIO.output(GPIO_LED, GPIO.LOW)
    GPIO.output(GPIO_RELAY, GPIO.LOW)


def read_temp():
    if open("/proc/version", 'r').read().find("piCore") == -1:
        return "66.6"
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()

    if lines[0].strip()[-3:] != 'YES':
        return "FAIL"
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return "{:2.1f}".format(temp_c)
    return "FAIL"


def send_temp(t):
    data = "value=" + str(t)
    url = "https://io.adafruit.com/api/v2/alexsartori/feeds/lil-t-feed/data"
    os.system("curl -H 'X-AIO-Key: " + ADAFRUIT_IO_KEY + "' -X POST -d " + data + ' ' + url)
